

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class RSAPlusAES {

    static private void processFile(Cipher ci, InputStream in, OutputStream out)
            throws javax.crypto.IllegalBlockSizeException,
            javax.crypto.BadPaddingException,
            java.io.IOException {
        byte[] ibuf = new byte[1024];
        int len;
        while ((len = in.read(ibuf)) != -1) {
            byte[] obuf = ci.update(ibuf, 0, len);
            if (obuf != null) out.write(obuf);
        }
        byte[] obuf = ci.doFinal();
        if (obuf != null) out.write(obuf);
        in.close();
    }

    /**
     * Call this method to initialize HyperLog.
     * By default, seven days older logs will gets deleted automatically.
     *
     * @param inFile  The inputFile, inside private app storage,
     * @param stringRSAPublicKey The String of the Public RSA key to encrypt the message.
     */
    static public File doEncryptFileRSAWithAES(File inFile, String stringRSAPublicKey)
            throws NoSuchAlgorithmException,
            java.security.InvalidAlgorithmParameterException,
            java.security.InvalidKeyException,
            javax.crypto.NoSuchPaddingException,
            javax.crypto.BadPaddingException,
            javax.crypto.IllegalBlockSizeException,
            java.io.IOException {

        //Create outputFIle based on inbound + "enc"
        File outFile = new File(inFile.getParent(), inFile.getName()+".enc");

        //LOAD PUBLIC KEY TO ENCRYPT
        PublicKey publicRSAKey = stringToPublicKey(stringRSAPublicKey);


        //GENERATE RANDOM AES KEY
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        SecureRandom srandom = new SecureRandom();
        kgen.init(256, srandom);
        SecretKey aesKey = kgen.generateKey();

        //GENERATE RANDOM INITVECTOR
        byte[] iv = new byte[128 / 8];
        srandom.nextBytes(iv);
        IvParameterSpec initvector = new IvParameterSpec(iv);

        try (FileOutputStream out = new FileOutputStream(outFile, true)) {

            //INIT RSA CYPHER WITH RSA PUBLIC KEY
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE, publicRSAKey);

            byte[] b = cipher.doFinal(aesKey.getEncoded());

            out.write(b);

            out.write(iv);

            Cipher ci = Cipher.getInstance("AES/CBC/PKCS5Padding");
            ci.init(Cipher.ENCRYPT_MODE, aesKey, initvector);
            try (FileInputStream in = new FileInputStream(inFile)) {
                processFile(ci, in, out);
            }
            out.flush();
        }
        return outFile;
    }

    static public File doDecryptRSAWithAES(File inFile, String stringRSAPrivateKey)
            throws NoSuchAlgorithmException,
            java.security.InvalidAlgorithmParameterException,
            java.security.InvalidKeyException,
            javax.crypto.NoSuchPaddingException,
            javax.crypto.BadPaddingException,
            javax.crypto.IllegalBlockSizeException,
            java.io.IOException {

        //Create outputFIle based on inbound + "enc"
        File outFile = new File(inFile.getParent(), inFile.getName()+".dec.txt");
        //Load Private KEY
        PrivateKey privateRSAKey = stringToPrivateKey(stringRSAPrivateKey);

        try (FileInputStream in = new FileInputStream(inFile)) {
            SecretKeySpec skey = null;
            {
                Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                cipher.init(Cipher.DECRYPT_MODE, privateRSAKey);
                byte[] b = new byte[256];
                in.read(b);
                byte[] keyb = cipher.doFinal(b);
                skey = new SecretKeySpec(keyb, "AES");
            }

            byte[] iv = new byte[128 / 8];
            in.read(iv);
            IvParameterSpec ivspec = new IvParameterSpec(iv);

            Cipher ci = Cipher.getInstance("AES/CBC/PKCS5Padding");
            ci.init(Cipher.DECRYPT_MODE, skey, ivspec);

            try (FileOutputStream out = new FileOutputStream(outFile)) {
                processFile(ci, in, out);
            }
        }
        return outFile;
    }

    private static PublicKey stringToPublicKey(String publicKeyString) {
        try {
            if (publicKeyString.contains("-----BEGIN PUBLIC KEY-----") || publicKeyString.contains("-----END PUBLIC KEY-----"))
                publicKeyString = publicKeyString.replace("-----BEGIN PUBLIC KEY-----", "").replace("-----END PUBLIC KEY-----", "");
            byte[] keyBytes = Base64.decode(publicKeyString);
            X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            return keyFactory.generatePublic(spec);

        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static PrivateKey stringToPrivateKey(String privateKeyString){
        try {
            if (privateKeyString.contains("-----BEGIN PUBLIC KEY-----") || privateKeyString.contains("-----END PUBLIC KEY-----"))
                privateKeyString = privateKeyString.replace("-----BEGIN PUBLIC KEY-----", "").replace("-----END PUBLIC KEY-----", "");
            byte[] keyBytes = Base64.decode(privateKeyString);
            PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PrivateKey thekey = keyFactory.generatePrivate(spec);
            return thekey;

        } catch (NoSuchAlgorithmException | InvalidKeySpecException | NullPointerException e) {
            e.printStackTrace();
            return null;
        }
    }
}
