import javafx.scene.control.Alert;
import javafx.stage.FileChooser;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class example1 {
    private JPanel panel1;
    private JButton bChooseFile;
    private JTextPane edPrivateKey;

    public example1() {
        decryptFile();
    }

    private void decryptFile(){
        bChooseFile.addActionListener(e -> {

            if (edPrivateKey.getText().trim().isEmpty()){
                JOptionPane.showMessageDialog(null,
                        "Empty Key please, use the private key.",
                        "Empty Key",
                        JOptionPane.WARNING_MESSAGE);
                return;
            }

            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("SelectEncripted Log");
            int seleccion = fileChooser.showOpenDialog(panel1);
            if (seleccion == JFileChooser.APPROVE_OPTION){
                String RsaPrivateKey = edPrivateKey.getText();
                try {
                    File output = RSAPlusAES.doDecryptRSAWithAES(fileChooser.getSelectedFile(), RsaPrivateKey.trim());
                    String text = "File Decrypted Coorrectly on: "+ output.getPath();
                    JOptionPane.showMessageDialog(null,
                            text,
                            "File Decrypt",
                            JOptionPane.INFORMATION_MESSAGE);
                } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException | InvalidKeyException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException | IOException ex) {
                    ex.printStackTrace();
                    String errorMessage = "Exception: " + ex.getLocalizedMessage();
                    JOptionPane.showMessageDialog(null,
                            errorMessage,
                            "File Decrypt",
                            JOptionPane.ERROR_MESSAGE);
                }
            }else{
                JOptionPane.showMessageDialog(null,
                        "Error Obtaning the file",
                        "File Decrypt",
                        JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("APP");
        frame.setContentPane(new example1().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
